# ai-softman

#### 介绍
AI软件人(AI Softman) 在AI基础上生成各类软件项目基础工程结构，包含各类中间件，构建工具等；可预览可下载完整工程包。

支持各类主流开发语言（java, python, sql, js, css等），界面可视化配置，自然语言输入。

目的是站在过去的肩膀上，将过去的经验，沉淀成一套完整的软件工程结构，并接合AI技术的优势，
让软件工程师更加专注于上层业务开发，而不是重复造轮子去关注底层的架构与设计。


#### 软件架构
- 使用SpringCloud+Nacos 组合微服务架构
- 兼容不同开发语言，实现各个语言的工程生成(对应一个微服务模块组件)



#### 主要功能
- 项目工程结构生成
- 项目工程结构预览
- 项目工程结构下载
- 项目工程结构配置

- 项目工程代码生成
- 项目工程代码预览
- 项目工程代码下载
- 项目工程代码配置



#### 主要技术栈

1. OpenJDK 17
2. SpringBoot 3.1.5
3. SpringCloud 2022.0.4
4. SpringCloud Alibaba 2022.0.3
5. Nacos 2.2.3
6. MySQL 5.7+
7. Redis 6.0+
8. Docker 20.10.7
9. Maven 3.6.0


#### 使用说明

前提：需要有OpenAI的API Key（或其他AI厂商的API Key）。

1.  下载项目源码
2.  导入IDEA，配置JDK，配置Maven
3.  打开项目，修改application.properties中openai.key为自己申请的API Key，修改数据库配置等
4.  运行项目，访问localhost:8080即可



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

